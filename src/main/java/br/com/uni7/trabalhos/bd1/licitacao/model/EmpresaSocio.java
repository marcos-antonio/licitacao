package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter
public class EmpresaSocio {

    @Id
    private String cnpj;

    @Id
    private String cpf;

    private String participacao;
}
