package br.com.uni7.trabalhos.bd1.licitacao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultasRepository extends JpaRepository<Object, Integer> {

    @Query(value = "select numero, descricao_item, data_inicio_inscricao, data_fim_inscricao, data_abertura, gg.nome, pregoeiro.nome from edital e\n" +
            "inner join funcionario gg on e.gerente_geral_solicitante_fk = gg.matricula\n" +
            "inner join funcionario pregoeiro on pregoeiro.matricula = e.pregoeiro_fk\n" +
            "where data_inicio_inscricao >= CURRENT_TIMESTAMP\n" +
            "order by e.data_lancamento desc;", nativeQuery = true)
    List<Object> consulta1();

}
