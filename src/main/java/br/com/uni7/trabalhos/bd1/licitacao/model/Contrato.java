package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class Contrato {

    @Id
    private Integer numero;

    @Id
    private Integer ano;
    private Edital edital;
    private String cnpjEmpresa;
    private LocalDateTime dataContrato;
    private LocalDateTime dataVigencia;
    private Double valorContrato;
}
