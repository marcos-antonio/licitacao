package br.com.uni7.trabalhos.bd1.licitacao.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter
public class Empresa {

    @Id
    @Column(length =  14)
    private String cnpj;

    private String razaoSocial;
    private String atividadeEconomica;
    private Double valorCapital;
    private Double valorFaturamento;
}
