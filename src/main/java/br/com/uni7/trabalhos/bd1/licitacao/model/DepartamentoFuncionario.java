package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class DepartamentoFuncionario {

    @Id
    private Integer id;
    private Departamento departamento;
    private Funcionario funcionario;
    private LocalDateTime dataCriacao;
}
