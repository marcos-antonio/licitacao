package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter
public class Departamento {

    @Id
    private Integer id;

    @Column(length = 5)
    private String sigla;
    private String descricao;
    private String localizacao;
    private Funcionario gerenteGeral;
    private Funcionario gerenteAdjunto;
    private Funcionario recepcionista;
}
