package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
public class Edital {

    @Id
    private Integer numero;

    @Id
    private Integer ano;

    private String descricaoItem;
    private LocalDateTime dataLancamento;
    private LocalDateTime dataInicioInscricao;
    private LocalDateTime dataFimInscricao;
    private LocalDateTime dataAbertura;
    private Departamento departamentoSolicitante;
    private Funcionario gerenteGeralSolicitante;
    private Funcionario gerenteAdjuntoSolicitante;
    private Funcionario pregoeiro;
}
