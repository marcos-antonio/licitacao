package br.com.uni7.trabalhos.bd1.licitacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicitacaoApplication {

    @Autowired
    private static ConsultasRepository repo;

    public static void main(String[] args) {
        SpringApplication.run(LicitacaoApplication.class, args);

        repo.consulta1();
    }

}
