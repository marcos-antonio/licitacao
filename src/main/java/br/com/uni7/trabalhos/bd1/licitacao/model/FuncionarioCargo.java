package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter @Setter
public class FuncionarioCargo {

    @Id
    private Integer id;

    private Funcionario funcionario;
    private Cargo cargo;
    private LocalDate dataCriacao;
}
