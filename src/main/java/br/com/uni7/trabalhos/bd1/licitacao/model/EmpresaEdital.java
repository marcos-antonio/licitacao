package br.com.uni7.trabalhos.bd1.licitacao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter
public class EmpresaEdital {

    @Id
    private Edital edital;

    @Id
    private Empresa empresa;

    private Double valorLance;
    private Integer ordem;

}
