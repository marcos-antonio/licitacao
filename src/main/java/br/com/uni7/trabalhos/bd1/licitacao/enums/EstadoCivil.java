package br.com.uni7.trabalhos.bd1.licitacao.enums;

public enum EstadoCivil {
    SOLTEIRO,
    CASADO,
    SEPARADO,
    DIVORCIADO,
    VIUVO
}
