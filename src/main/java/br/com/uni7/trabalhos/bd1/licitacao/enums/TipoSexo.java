package br.com.uni7.trabalhos.bd1.licitacao.enums;

public enum TipoSexo {
    MASCULINO,
    FEMININO,
    INDEFINIDO

}
