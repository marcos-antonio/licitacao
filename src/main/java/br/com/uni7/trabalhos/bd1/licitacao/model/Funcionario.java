package br.com.uni7.trabalhos.bd1.licitacao.model;


import br.com.uni7.trabalhos.bd1.licitacao.enums.TipoSexo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;

@Entity
@Getter @Setter
public class Funcionario {

    @Id
    private String matricula;

    private String nome;
    private String cpf;
    private String rg;
    private LocalDate dataNascimento;
    private String filiacao;
    private String endereco;

    @Enumerated(value = EnumType.ORDINAL)
    private TipoSexo sexo;

    private Collection<FuncionarioEstadoCivil> estadosCivis;
    private Collection<FuncionarioCargo> cargos;

}
