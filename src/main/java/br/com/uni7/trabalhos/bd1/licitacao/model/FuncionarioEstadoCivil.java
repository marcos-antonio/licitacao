package br.com.uni7.trabalhos.bd1.licitacao.model;

import br.com.uni7.trabalhos.bd1.licitacao.enums.EstadoCivil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter @Setter
public class FuncionarioEstadoCivil {

    @Id
    private Integer idFuncionarioEstadoCivil;
    private Funcionario funcionario;

    @Enumerated(EnumType.ORDINAL)
    private EstadoCivil estadoCivil;
    private LocalDate dataCriacao;

}
